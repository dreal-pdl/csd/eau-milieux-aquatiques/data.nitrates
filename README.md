
<!-- README.md is generated from README.Rmd. Please edit that file -->

# data.nitrates

<!-- badges: start -->

[![Latest
Release](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates/-/badges/release.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates/-/releases)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Lifecycle:
stable](https://img.shields.io/badge/lifecycle-stable-green.svg)](https://lifecycle.r-lib.org/articles/stages.html#stable)
[![pipeline
status](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates/badges/master/pipeline.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates/-/commits/master)

<!-- badges: end -->

## Objectif

L’objectif du package `data.nitrates` est de faciliter l’actualisation
des lots de données suivants dans une base de données PostgreSQL locale.

**Sources :**

- Données de l’ARS : `nitrates.nitrate_data_analyse_ars`
- Données provenant de la plateforme Hub’eau :
  - [API “Qualité des nappes d’eau
    souterraine”](https://hubeau.eaufrance.fr/page/api-qualite-nappes) :
    `qualite_nappes_eau_souterraine.nitrate_qualite_nappes_analyses`
  - [API “Qualité des cours
    d’eau”](https://hubeau.eaufrance.fr/page/api-qualite-cours-deau) :
    `qualite_cours_d_eau.hubeau_qualite_rivieres_station_pc`

**Livrables :**

- Table des prélèvements : `nitrates.nitrate_prelevement`
- Table des analyses : `nitrates.nitrate_analyse`

## Installation

Installer le package `remotes` si besoin :

``` r
install.packages("remotes")
```

Installer le package `data.nitrates` :

``` r
remotes::install_gitlab('dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates', host="gitlab-forge.din.developpement-durable.gouv.fr")
```

## Utilisation

### Chargement du package

Charger le package `data.nitrates` dans la session :

``` r
library(data.nitrates)
```

### Documentation

La documentation du package est consultable sur ce site :
<https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/eau-milieux-aquatiques/data.nitrates/>

### Chargement d’un template

Ouvrir un nouveau fichier RMarkdown via *New File \> Rmarkdown* :

<figure>
<img
src="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates/-/raw/master/inst/images/new_file_rmarkdown.png"
alt="Capture d’écran du menu" />
<figcaption aria-hidden="true">Capture d’écran du menu</figcaption>
</figure>

Sélectionner *From Template* dans la fenêtre puis le template souhaité
parmi ceux proposés pour {data.nitrates} :

<figure>
<img
src="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.nitrates/-/raw/master/inst/images/from_template.png"
alt="Capture d’écran de la fenêtre" />
<figcaption aria-hidden="true">Capture d’écran de la
fenêtre</figcaption>
</figure>

Un nouveau fichier .Rmd est créé à partir du template et peut être
enregistré par l’utilisateur sur son poste de travail.

Il faut ensuite suivre les différentes étapes (en adaptant si besoin les
paramètres) pour mettre à jour le lot de données concerné.

### Ordre des scripts

| Ordre | Nom du Script                                 | Description                                                                    |
|-------|-----------------------------------------------|--------------------------------------------------------------------------------|
| 1     | `flat_list_existing_tables.Rmd`               | Visualise les tables existantes (optionnel)                                    |
| 2     | `flat_create_tables_sequences.Rmd`            | Créé les tables de la nouvelle version                                         |
| 3     | `flat_import_ars_data.Rmd`                    | Importe les données de l’ARS                                                   |
| 4     | `flat_import_hubeau_eso_data.Rmd`             | Importe les données Eau souterraine à partir de Hubeau                         |
| 5     | `flat_import_hubeau_esu_data.Rmd`             | Importe les données Eau de surface à partir de Hubeau                          |
| 6     | `flat_insert_ars_into_prelevement.Rmd`        | Importe les prélèvements ARS dans la table des prélèvements                    |
| 7     | `flat_insert_hubeau_eso_into_prelevement.Rmd` | Importe les prélèvements HUbeau Eau souterraine dans la table des prélèvements |
| 8     | `flat_insert_hubeau_esu_into_prelevement.Rmd` | Importe les prélèvements Hubeau Eau de surface dans la table des prélèvements  |
| 9     | `flat_insert_ars_into_analyse.Rmd`            | Importe les analyses ARS dans la table des analyses                            |
| 10    | `flat_insert_hubeau_eso_into_analyse.Rmd`     | Importe les analyses HUbeau Eau souterraine dans la table des analyses         |
| 11    | `flat_insert_hubeau_esu_into_analyse.Rmd`     | Importe les analyses Hubeau Eau de surface dans la table des analyses          |
