# data.nitrates 1.2.0

* Utilisation du fichier config.yml pour stocker les paramètres version, last_year et filepath_data_ars
* Modification des fichiers .Rmd utilisant ces paramètres
* Rétablissement du code `nature_eau` correct pour les prélèvements des stations ESU
* Correction du bug de génération des skeletons

# data.nitrates 1.1.0

* Ajout du champs `nature_eau` (ESO/ESU) dans la table de prélèvements
* Remplacement des codes SISE-EAUX par les codes BSS pour les prélèvements ARS ESO 

# data.nitrates 1.0.0

* Finalisation des vignettes de chargement des données consolidées pour les analyses
* Ajout des templates RMarkdown
* Ajout de la procédure d'utilisation dans le README

# data.nitrates 0.3.0

* Finalisation des vignettes de chargement des données consolidées pour les prélèvements

# data.nitrates 0.2.0

* Finalisation des vignettes "Import des données Hub'eau Cours d'eau" et "Import des données Hub'eau Nappes d'eau souterraine"

# data.nitrates 0.1.0

* Finalisation de la vignette "Import des données ARS"
