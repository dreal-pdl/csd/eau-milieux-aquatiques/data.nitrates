---
title: "Insertion des analyses Hubeau ESU"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(yaml)
library(datalibaba)
library(dplyr)
library(stringr)
library(glue)
library(DBI)
library(RPostgres)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

```{r config, eval=FALSE}
# Lire le fichier de configuration
config <- yaml::read_yaml("config.yml")

# Accéder à la valeur pour version
version <- config$version
```

# Consolidation et insertion des analyses Hub'eau ESU dans la table des analyses

## Chargement des analyses Hub'eau ESU

La table des données brutes Nitrates Hub'eau ESU est chargée :
```{r load-nitrate_qualite_rivieres_analyse_pc, eval=FALSE}
# Charger la table qualite_cours_d_eau.nitrate_qualite_rivieres_analyse_pc
nitrate_qualite_rivieres_analyses <- datalibaba::importer_data(
  table = "nitrate_qualite_rivieres_analyse_pc",
  schema = "qualite_cours_d_eau",
  db = "si_eau",
  user = "admin"
)
```

## Consolidation des analyses Hub'eau ESU

On remplace des valeurs dans limite_detection et limite_quantification :
```{r replace-dot_limite_detection, eval=FALSE}
# Remplacer les valeurs dans les colonnes limite_detection et limite_quantification
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::mutate(
    resultat = stringr::str_replace(resultat, "\\,", "."),
    limite_detection = stringr::str_replace(limite_detection, "\\,", "."),
    limite_quantification = stringr::str_replace(limite_quantification, "\\,", ".")
    )

```

On sélectionne les champs utiles à la table des analyses :
```{r select-variables-hubeau_esu, eval=FALSE}
# Sélectionner les variables
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::select(code_station,
                code_intervenant = code_laboratoire,
                date_prelevement,
                date_analyse,
                resultat_analyse = resultat,
                code_parametre,
                code_fraction_analysee = code_fraction,
                code_remarque,
                limite_detection,
                limite_quantification)

```

On modifie le type des variables  resultat_analyse et  limite_quantification :
```{r change-fieldtypes, eval=FALSE}
# Remplacer les valeurs dans les colonnes resultat_analyse et limite_quantification
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::mutate(resultat_analyse = as.numeric(resultat_analyse),
                limite_detection = as.numeric(limite_detection),
                limite_quantification = as.numeric(limite_quantification),
                code_parametre = as.integer(code_parametre),
                code_fraction_analysee  = as.integer(code_fraction_analysee),
                code_remarque = as.integer(code_remarque)
                )

```

# Jointure avec la table des prélèvements créée auparavant

La table des prélèvements est chargée :
```{r load-nitrate_prelevement, eval=FALSE}
# Charger la table nitrates.nitrate_prelevement_version
nitrate_prelevement <- datalibaba::importer_data(
  table = glue::glue("nitrate_prelevement_", version),
  schema = "nitrates",
  db = "si_eau",
  user = "admin"
)

```

On dédoublonne les lignes en utilisant les champs `code_station` et `date_prelevement` 
afin de ne conserver qu'un prélèvement par station et date donnée :
```{r select-distinct-rows_hubeau_esu, eval=FALSE}
# Dédoublonner les lignes sur les colonnes code_station et date_prelevement
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::distinct(code_station, date_prelevement, .keep_all = TRUE)

```

On joint le dataframe des prélèvements pour récupérer la variable code_prelevement :
```{r join-prelevement_hubeau_esu, eval=FALSE}
# Joindre les dataframes nitrate_qualite_rivieres_analyses et nitrate_prelevement
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::left_join(nitrate_prelevement |>
                     dplyr::select(code_station, date_prelevement, code_prelevement), 
                   by = c("code_station" = "code_station", "date_prelevement" = "date_prelevement"))
```

On dédoublonne les lignes en utilisant les champs `code_prelevement`,
`code_parametre` et `resultat_analyse` afin de ne conserver qu'une analyse :
```{r select-distinct-rows_hubeau_esu_2, eval=FALSE}
# Dédoublonner les lignes sur les colonnes code_station et date_prelevement
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::distinct(code_prelevement, code_parametre, resultat_analyse, .keep_all = TRUE)

```

On ajoute un identifiant unique s'appuyant sur une séquence stockée en base :
```{r add_code_analyse_hubeau_esu, eval=FALSE}
# Utiliser la fonction add_code_analyse() avec la version souhaitée
nitrate_qualite_rivieres_analyses <- add_code_analyse(
  nitrate_qualite_rivieres_analyses, version)

# Afficher le dataframe pour vérifier les modifications
print(nitrate_qualite_rivieres_analyses)

```

# Chargement en base

On corrige l'ordre des champs les champs utiles à la table des analyses :
```{r select-variables-hubeau_esu_final, eval=FALSE}
# Sélectionner les variables dans l'ordre des champs de la table à alimenter
nitrate_qualite_rivieres_analyses <- nitrate_qualite_rivieres_analyses |>
  dplyr::select(code_analyse,
                code_intervenant,
                code_prelevement,
                code_parametre,
                code_fraction_analysee,
                date_analyse,
                resultat_analyse,
                code_remarque,
                limite_detection,
                limite_quantification)

```

On charge les données consolidées dans une table dédiée :
```{r insert-into_nitrate_analyse_hubeau_esu_version, eval=FALSE}
# Charger les données dans une nouvelle table en base
datalibaba::poster_data(data = nitrate_qualite_rivieres_analyses, 
                        table = glue::glue("nitrate_analyse_hubeau_esu_", version),
                        schema = "nitrates", 
                        db = "si_eau",
                        overwrite = TRUE,
                        pk = "code_analyse",
                        user = "admin")
```

# Insertion des analyses Hub'eau ESU en base dans la table globale

On insère enfin les enregistrements de cette table dans la table globale :
```{r import_and_merge_tables_hubeau_esu, eval=FALSE}
# Insérer les données de la table du dernier millésime vers la table complète
collectr::import_and_merge_tables(database = "si_eau",
                                  source_table = glue::glue("nitrate_analyse_hubeau_esu_", version),
                                  source_schema = "nitrates", 
                                  target_table = glue::glue("nitrate_analyse_", version),
                                  target_schema = "nitrates",
                                  role = "admin")

```

```{r development-skeleton-dir, eval=FALSE}
# Créer de l'arborescence et des fichiers du template
usethis::use_rmarkdown_template(
  template_name = "Insertion des analyses Hubeau ESU",
  template_dir = "insertion-des-analyses-hubeau-esu",
  template_description = "Insertion des analyses Hubeau ESU",
  template_create_dir = TRUE
)

```

```{r development-skeleton-copy, eval=FALSE}
# Définir les chemins source et destination
source_file <- "dev/flat_insert_hubeau_esu_into_analyse.Rmd"
destination_dir <- "inst/rmarkdown/templates/insertion-des-analyses-hubeau-esu/skeleton"
destination_file <- file.path(destination_dir, "skeleton.Rmd")

# Copier et renommer le fichier
file.copy(from = source_file, to = destination_file, overwrite = TRUE)
message("File copied and renamed successfully.")

```

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_insert_hubeau_esu_into_analyse.Rmd", vignette_name = "Insertion des analyses Hubeau ESU")
```

